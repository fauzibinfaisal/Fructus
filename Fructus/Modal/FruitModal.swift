//
//  FruitModal.swift
//  Fructus
//
//  Created by Gop-c2s2-f on 01/06/21.
//

import SwiftUI

//MARK: - FRUITS DATA MODAL

struct Fruit: Identifiable {
    var id = UUID()
    var title: String
    var headline: String
    var image: String
    var gradientColors: [Color]
    var description: String
    var nutrition: [String]
}
