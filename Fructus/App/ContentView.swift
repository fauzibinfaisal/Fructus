//
//  ContentView.swift
//  Fructus
//
//  Created by Gop-c2s2-f on 29/05/21.
//

import SwiftUI

struct ContentView: View {
    //MARK: - PROPERTIES
    
    @State private var isShowingSettings: Bool = false
    var fruits: [Fruit] = fruitsData

    //MARK: - BODY
    
    var body: some View {
        NavigationView{
            List {
                ForEach(fruits.shuffled()) { item in
                    NavigationLink(
                        destination: FruitsDetailView(fruit: item)){
                    FruitRowView(fruit: item).padding(.vertical, 4)
                    }
                }
            }
            .navigationTitle("Fruits")
            .navigationBarItems(
            trailing:
                Button(action: {
                    isShowingSettings  = true
                }) {
                    Image(systemName: "slider.horizontal.3")
                }  //: BUTTON
                .sheet(isPresented: $isShowingSettings){
                    SettingsView()
                }
            )
        }
    }
}

//MARK: PREVIEW

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(fruits: fruitsData)
    }
}
