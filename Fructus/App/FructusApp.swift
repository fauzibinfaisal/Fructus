//
//  FructusApp.swift
//  Fructus
//
//  Created by Gop-c2s2-f on 29/05/21.
//

import SwiftUI

@main
struct FructusApp: App {
    @AppStorage("isOnboarding") var isOnboarding: Bool = true
    
    var body: some Scene {
        WindowGroup {
            if  isOnboarding{
                OnboardingView()
            } else {
                ContentView()
            }
        }
    }
}
